package com.example.rany.multilanguagedemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.akexorcist.localizationactivity.core.OnLocaleChangedListener;

import java.util.Locale;

public class MainActivity extends BaseActivity {

    private static final String TAG = "ooooo" ;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setDefaultLanguage(Locale.KOREAN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item, menu);
        return super.onCreateOptionsMenu(menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.itm_en:
                setLanguage("en");
                break;
            case R.id.itm_km:
                setLanguage("km");
                break;
            case R.id.itm_ko:
                setLanguage("ko");
                break;

        }
        return super.onOptionsItemSelected(item);
    }
}
